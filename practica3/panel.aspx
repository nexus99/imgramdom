﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="panel.aspx.cs" Inherits="practica3.registros" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registros</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <asp:LinkButton ID="lblCerrar" runat="server" OnClick="lblCerrar_Click">Cerrar sesión</asp:LinkButton>
        <div class="row">
            <div class="col-12">
                <h5>Sube una foto</h5>
                <div class="form-group">
                    <asp:Label Text="Nombre de la imagen" runat="server" />
                    <asp:TextBox ID="txtImgName" required ="required" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblFu" runat="server" Text="Seleccione un archivo"></asp:Label>
                    <asp:FileUpload ID="fuImg" CssClass="form-control-file" runat="server" />
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubir" CssClass="btn btn-primary" runat="server" Text="Subir Foto" OnClick="btnSubir_Click" />
                </div>
                
            </div>
        </div>       
         
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <h4>Archivo Seleccionado</h4>
                    <asp:Label ID="lblNombre" Text="No hay Ningun Archvio selecinado" runat="server" />
                    <br />
                    <asp:Label ID="lblID" Text="" runat="server" />
                    <br />
                    <br />
                    <div class="btn-group">
                        <asp:LinkButton ID="lkEliminar" CssClass="btn btn-danger" runat="server" OnClick="lkEliminar_Click">Eliminar Imagen</asp:LinkButton>
                        <asp:LinkButton ID="lkDeshabilitar" CssClass="btn btn-secondary" runat="server" OnClick="lkDeshabilitar_Click">Deshabilitar Imagen</asp:LinkButton>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h5>Tus imagenes</h5>
                <asp:LinkButton ID="lbActualizar" runat="server" OnClick="lbActualizar_Click">Actualiza Tabla</asp:LinkButton>
                <asp:GridView ID="gvImagenes" DataKeyNames="id_imagen" CssClass="table table-hover" AutoGenerateColumns="false" OnSelectedIndexChanged="gvImagenes_SelectedIndexChanged1" runat="server" >
                    <Columns>
                        <asp:BoundField DataField="id_imagen" HeaderText="ID" />
                        <asp:BoundField DataField="nombre" HeaderText="Nombre de la imagen" />
                        <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha de registro" />
                        <asp:BoundField DataField ="likes" HeaderText="Total de likes" />
                        <asp:BoundField DataField="dislikes" HeaderText ="Total de dislikes" />
                        <asp:BoundField DataField="total" HeaderText="Total" />
                        <asp:BoundField DataField="estatus" HeaderText="Estatus" />
                        <asp:ButtonField ButtonType="Link" CommandName="Select" HeaderText="opciones"  Text="Seleccionar" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    </form>
     
</body>
</html>
