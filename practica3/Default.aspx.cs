﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using practica3.Models;
namespace practica3
{
    public partial class Default : System.Web.UI.Page
    {
        Imagen imagen;

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                imagen = new Imagen();
                img.ImageUrl = imagen.ImagenRandom(); 
            }
        }

        protected void lbLike_Click(object sender, EventArgs e)
        {
            imagen = new Imagen();
            img.ImageUrl = imagen.ImagenRandom(); 
            imagen.like = true;
            imagen.sumarLikes();
        }

        protected void lbDislike_Click(object sender, EventArgs e)
        {
            imagen = new Imagen();
            img.ImageUrl = imagen.ImagenRandom(); 
            imagen.like = false;
            imagen.sumarLikes();
        }

     
    }
}