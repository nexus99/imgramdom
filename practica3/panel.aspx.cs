﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using practica3.Models;

namespace practica3
{
    public partial class registros : System.Web.UI.Page
    {
        Imagen img;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["id_usuario"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            img = new Imagen();
            img.id_usuario = (int)(Session["id_usuario"]);
            gvImagenes.DataSource = img.traerImagenes();
            gvImagenes.DataBind();
            
        }

        protected void btnSubir_Click(object sender, EventArgs e)
        {
            if (fuImg.HasFile)
            {
                string ruta = Server.MapPath(".");
                ruta += @"\Imagenes\" + fuImg.FileName;               
                fuImg.SaveAs(ruta);
                img.nombre = txtImgName.Text.Trim();
                img.ruta = "Imagenes/" + fuImg.FileName;
                img.guardar();      
                
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                       "alert('No se ha seleccionado ninguna imagen')", true);
            }
        }

        protected void lbActualizar_Click(object sender, EventArgs e)
        {
            gvImagenes.DataSource = img.traerImagenes();
        }


        protected void gvImagenes_SelectedIndexChanged1(object sender, EventArgs e)
        {
            GridViewRow fila = gvImagenes.SelectedRow;
            lblNombre.Text = fila.Cells[1].Text;
            lblID.Text = fila.Cells[0].Text;
            img.id_imagen = int.Parse(fila.Cells[0].Text);
        }

        protected void lblCerrar_Click(object sender, EventArgs e)
        {
            Session["id_usuario"] = null;
            Response.Redirect("Default.aspx");
        }

        protected void lkEliminar_Click(object sender, EventArgs e)
        {
            if (lblNombre.Text != "No hay Ningun Archvio selecinado")
            {
                img.id_imagen = int.Parse(lblID.Text);
                img.elmininarImagen();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                       "alert('No ha seleccionado ninguna fila')", true);
            }
        }

        protected void lkDeshabilitar_Click(object sender, EventArgs e)
        {
            if (lblNombre.Text != "No hay Ningun Archvio selecinado")
            {
                img.id_imagen = int.Parse(lblID.Text);
                img.desahabilitarImagen();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                       "alert('No ha seleccionado ninguna fila')", true);
            }
        }

     

    
    }
}