﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using practica3.Models;

namespace practica3
{
    public partial class login : System.Web.UI.Page
    {
        Usuario usu;
        protected void Page_Load(object sender, EventArgs e)
        {
            usu = new Usuario();
        }

        protected void btnIniciar_Click(object sender, EventArgs e)
        {
            usu.usuario = txtUsuario.Text.Trim();
            usu.contrasena = txtContrasena.Text;            
            if (usu.validarAcceso())
            {
                Session["id_usuario"] = usu.id_usuario;
                Response.Redirect("panel.aspx");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                        "alert('Usuario y/o contraseña incorrectos')", true);
            }
        }
    }
}