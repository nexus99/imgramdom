﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using practica3.Models;

namespace practica3
{
    public partial class registro : System.Web.UI.Page
    {
        Usuario usu;
        protected void Page_Load(object sender, EventArgs e)
        {
            usu = new Usuario();
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            usu.usuario = txtUsuario.Text.Trim();
            usu.contrasena = txtContrasena.Text;
            if (!usu.usuarioExistente())
            {
                if (txtContrasena.Text == txtConfrimarContrasena.Text)
                {
                    usu.guardar();
                    Response.Redirect("login.aspx");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                        "alert('las contraseñas no coinciden')", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                        "alert('El usuario ya se encuentra registrado, prueba con otro')", true);
            }
        }
    }
}