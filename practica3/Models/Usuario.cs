﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using practica3.DAO;
using practica3.Utelerias;

namespace practica3.Models
{
    public class Usuario: Conexion
    {
        public int id_usuario { get; set; }
        public string usuario { get; set; }
        public string contrasena { get; set; }
        string query;

        public bool usuarioExistente()
        {
            try
            {
                query = "SELECT * FROM usuarios WHERE usuario = @usuario";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@usuario",usuario);
                return (Consulta(cmd).Rows.Count > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int guardar()
        {
            try
            {
                query = "INSERT INTO usuarios(usuario,contrasena) VALUES (@usuario,@contrasena)";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.Parameters.AddWithValue("@Contrasena", Encriptar.GetSHA1(contrasena));
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool validarAcceso()
        {
            try
            {
                query = "SELECT * FROM usuarios WHERE usuario = @usuario AND contrasena = @contrasena";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.Parameters.AddWithValue("@contrasena", Encriptar.GetSHA1(contrasena));
                DataTable usu = Consulta(cmd);
                // primero se recupera la fila y posterior la el valor a recuperar
                if (usu.Rows.Count > 0) {
                    id_usuario = (int)(usu.Rows[0]["id_usuario"]);
                }
                return (usu.Rows.Count > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}