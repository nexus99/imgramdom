﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using practica3.DAO;
using System.Data;
using System.Data.SqlClient;

namespace practica3.Models
{
    public class Imagen: Conexion
    {
        public int id_usuario { get; set; }
        public int id_imagen { get; set; }
        public string nombre { get; set; }
        public string ruta { get; set; }
        public bool like { set; get; }
        private int likes { set; get; }
        private int dislikes { set; get; }
        private int total { set; get; }
        public int guardar()
        {
            try
            {
                string query = "INSERT INTO imagenes(id_usuario,nombre,ruta) VALUES(@id_usuario,@nombre,@ruta)";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@id_usuario", id_usuario);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@ruta", ruta);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int elmininarImagen()
        {
            try
            {
                string query = "DELETE FROM imagenes WHERE id_imagen=@id_imagen";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@id_imagen", id_imagen);
                return Ejecutar(cmd);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int desahabilitarImagen()
        {
            try
            {
                string query = "UPDATE imagenes SET id_estatus=2 WHERE id_imagen= @id_imagen";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@id_imagen", id_imagen);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable traerImagenes()
        {
            try
            {
                string query = "SELECT * FROM imagenes I INNER JOIN estatus E ON I.id_estatus = E.ID_estatus WHERE id_usuario = @id_usuario";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@id_usuario",id_usuario);
                return Consulta(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Retorna la ruta de una imagen a la sar
        /// </summary>
        /// <returns>string:ruta</returns>
        public string ImagenRandom()
        {
            try
            {
                string query = "SELECT * FROM imagenes WHERE id_estatus = 1";
                SqlCommand cmd = new SqlCommand(query);
                DataTable imagenes = Consulta(cmd);
                Random ran = new Random();
                int fila = ran.Next(0, imagenes.Rows.Count);
                // el primero corcheta representa que fila en su indice queremos recuperar, el segundo es la columna
                string ruta = imagenes.Rows[fila]["ruta"].ToString();
                id_imagen = (int) (imagenes.Rows[fila]["id_imagen"]);
                return ruta;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int sumarLikes()
        {
            try
            {
                string query = "SELECT likes,dislikes FROM imagenes WHERE id_imagen = @id_imagen";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@id_imagen", id_imagen);
                DataTable tabla = Consulta(cmd);
                likes = int.Parse(tabla.Rows[0]["likes"].ToString());  // RECUPERAMOS EL NUMERO DE LIKES QUE TENEMOS
                dislikes = int.Parse(tabla.Rows[0]["dislikes"].ToString());
                if (like)
                {
                    likes++;
                }
                else
                {
                    dislikes++;
                }
                total = likes + dislikes;
                query = "UPDATE imagenes SET likes = @likes, dislikes = @dislikes, total = @total WHERE id_imagen = @id_imagen";
                cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@dislikes", dislikes);
                cmd.Parameters.AddWithValue("@likes", likes);
                cmd.Parameters.AddWithValue("@total", total);
                cmd.Parameters.AddWithValue("@id_imagen", id_imagen);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}