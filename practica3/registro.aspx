﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registro.aspx.cs" Inherits="practica3.registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registro</title>    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <style>
        body {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display:flex;
            justify-content:center;
            align-items:center;
        }

        .carta-fix {
            width:25%;
            padding:24px;
        }
    </style>
</head>
<body class="bodyCenter">
    <form id="form1" class="card carta-fix" runat="server">
    <div>
        <div class="form-group">
            <asp:Label ID="lblUsuario" AssociatedControlID="txtUsuario" runat="server" Text="Usuario"></asp:Label>
            <asp:TextBox ID="txtUsuario" CssClass="form-control" required="required" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="lblContrasena" AssociatedControlID="txtContrasena" runat="server" Text="Constraseña"></asp:Label>
            <asp:TextBox ID="txtContrasena" required="required" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="lblCOnfirmarContrasena" AssociatedControlID="txtConfrimarContrasena" runat="server" Text="Confirmar Contraseña"></asp:Label>
            <asp:TextBox ID="txtConfrimarContrasena" required="required" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
        </div>
        
        <div class="form-group">
            <asp:Button ID="btnRegistrar" CssClass="btn btn-primary" runat="server" Text="Registrarse" OnClick="btnRegistrar_Click" />
            <asp:HyperLink ID="hlRegresar" CssClass="btn btn-secondary" NavigateUrl="~/Default.aspx" runat="server">volver</asp:HyperLink>
        </div>
        
    </div>
    </form>
</body>
</html>
