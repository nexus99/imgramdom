﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="practica3.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, user-scalable=1" />
    <title>Pagina De Inicio</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <style>
        .botones {
            padding:15px;
            display:flex;
            justify-content:space-around;
        }

        .container.img {
            padding-top:3%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark text-white">         
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <asp:HyperLink ID="hlLogin" CssClass="nav-link" runat="server" NavigateUrl="~/login.aspx">Iniciar Sesión</asp:HyperLink>            
                    </li>
                    <li class="nav-item">
                        <asp:HyperLink ID="hlRegistro" CssClass="nav-link" runat="server" NavigateUrl="~/registro.aspx">Registrarse</asp:HyperLink>           
                    </li>                    
                </ul>
            </div>
        </nav>
    <div class="contenedor">
        <div class="container img">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <asp:Image ID="img" CssClass="img-fluid" ImageUrl="Imagenes/2013DaftPunkVoguePress230713.jpg" runat="server" />
                    <div class="botones">
                        <asp:LinkButton CssClass="btn btn-primary" ID="lbLike" runat="server" OnClick="lbLike_Click">Me gusta</asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-primary" ID="lbDislike" runat="server" OnClick="lbDislike_Click">No me gusta</asp:LinkButton>
                    </div>
                </div>
            </div>
            

        </div>
        
    </div>
    </form>
</body>
</html>
