CREATE DATABASE practica3;
GO

USE practica3;
GO

CREATE TABLE usuarios
(
	id_usuario		INT IDENTITY(1,1) PRIMARY KEY,
	usuario			VARCHAR(250) UNIQUE NOT NULL,
	contrasena		VARCHAR(MAX) NOT NULL
);
GO

CREATE TABLE estatus
(
	ID_estatus	INT IDENTITY(1,1) PRIMARY KEY,
	estatus		VARCHAR(250) NOT NULL
);
GO

CREATE TABLE imagenes 
(
	id_imagen		INT IDENTITY(1,1) PRIMARY KEY,
	nombre			VARCHAR(350) NOT NULL,
	ruta			VARCHAR(350) NOT NULL,
	likes			INT DEFAULT(0),
	dislikes		INT DEFAULT(0), 
	total			INT DEFAULT(0),
	id_estatus		INT DEFAULT(1) FOREIGN KEY REFERENCES estatus(id_estatus), 
	fechaCreacion	DATE DEFAULT(GETDATE()),
	id_usuario		INT FOREIGN KEY REFERENCES usuarios(id_usuario)
);
GO

INSERT INTO estatus(estatus) VALUES ('habilitado'),('deshabilitado');
GO

